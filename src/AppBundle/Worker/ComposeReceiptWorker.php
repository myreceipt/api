<?php
/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 26/02/2018
 * Time: 22:08
 */

namespace AppBundle\Worker;


use AppBundle\Document\FiscalDocument;
use AppBundle\Entity\Company;
use AppBundle\Entity\Kkt;
use AppBundle\Extension\KktServerCommunicator;
use AppBundle\Model\ReceiptResponse;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Makasim\Yadm\Storage;
use MongoDB\BSON\ObjectId;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use AppBundle\Model\ReceiptRequest;

class ComposeReceiptWorker implements ConsumerInterface
{

    /** @var LoggerInterface */
    private $logger;
    /** @var EntityManager */
    private $em;
    /** @var KktServerCommunicator */
    private $communicator;
    /** @var ProducerInterface */
    private $producer;
    /** @var Serializer */
    private $serializer;
    /** @var Storage */
    private $storage;

    public function __construct(LoggerInterface $logger, EntityManager $em, KktServerCommunicator $communicator, ProducerInterface $producer, Serializer $serializer, Storage $storage)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->communicator = $communicator;
        $this->producer = $producer;
        $this->serializer = $serializer;
        $this->storage = $storage;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $id = $msg->getBody();
        $this->logger->pushProcessor(function (array $record) use ($id) {
            $record['context']['id'] = $id;
            $record['context']['worker'] = 'ComposeReceipt';
            return $record;
        });
        $this->logger->info('Start processing receipt');
        /** @var FiscalDocument $row */
        $row = $this->storage->findOne(['_id' => new ObjectId($id)]);
        $request = $this->serializer->fromArray($row->getRequest(), ReceiptRequest::class);
        $kkt = $this->chooseKkt($row->getInn());

        $result = $this->process($request, $kkt);
        $response = (new ReceiptResponse())->setMessage($result['message'])->setCode($result['code']);

        $row->setKkt($kkt->getSerialNumber())
            ->setKktResult($result)
            ->setResponse($this->serializer->toArray($response));
        $this->storage->update($row);

        $this->logger->info('Receipt processed', ['code' => $response->getCode()]);
        $this->logger->popProcessor();
        return true;
    }

    public function process(ReceiptRequest $request, Kkt $kkt)
    {
        // hack for demo kkt
        if ($kkt->getId() === 1) {
            $client = $request->getClient();
            $client->setEmail("support@check-sender.ru");
            $request->setClient($client);
        }

        $result = $this->communicator->receiptRegister($request, $kkt->getSerialNumber());
        return $result;
    }

    protected function chooseKkt($inn)
    {
        $repoC = $this->em->getRepository(Company::class);
        $company = $repoC->findOneBy(['inn' => $inn]);

        $repoK = $this->em->getRepository(Kkt::class);
        $kkt = $repoK->findOneBy(['company' => $company]);
        return $kkt;
    }

}