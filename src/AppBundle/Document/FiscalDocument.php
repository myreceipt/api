<?php
/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 27/04/2018
 * Time: 22:44
 */

namespace AppBundle\Document;

use function Makasim\Values\set_value;
use function Makasim\Values\get_value;
use function Makasim\Yadm\get_object_id;


class FiscalDocument
{

    private $values = [];

    public function getId()
    {
        return get_object_id($this);
    }

    public function getDocument()
    {
        return get_value($this, 'document');
    }

    public function setDocument($document)
    {
        set_value($this, 'document', $document);
        return $this;
    }

    public function getInn()
    {
        return get_value($this, 'inn');
    }

    public function setInn($inn)
    {
        set_value($this, 'inn', $inn);
        return $this;
    }

    public function getKkt()
    {
        return get_value($this, 'kkt');
    }

    public function setKkt($kkt)
    {
        set_value($this, 'kkt', $kkt);
        return $this;
    }

    public function getRequest()
    {
        return get_value($this, 'request');
    }

    public function setRequest($request)
    {
        set_value($this, 'request', $request);
        return $this;
    }

    public function getResponse()
    {
        return get_value($this, 'response');
    }

    public function setResponse($response)
    {
        set_value($this, 'response', $response);
        return $this;
    }

    public function getKktResult()
    {
        return get_value($this, 'kkt_result');
    }

    public function setKktResult($response)
    {
        set_value($this, 'kkt_result', $response);
        return $this;
    }

}