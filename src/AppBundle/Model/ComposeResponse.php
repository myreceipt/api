<?php
namespace AppBundle\Model;


use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class ComposeResponse
{
        /**
     * @var string|null
     * @SerializedName("status")
     * @Assert\Choice({ "pending", "failure", "success" })
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $status;

    /**
     * Дата принятия документа на регистрацию
     *
     * @var string|null
     * @SerializedName("datetime")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $datetime;

    /**
     * Уникальный идентификатор документа в системе Check-Sender
     *
     * @var string|null
     * @SerializedName("uuid")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $uuid;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->status = isset($data['status']) ? $data['status'] : null;
        $this->datetime = isset($data['datetime']) ? $data['datetime'] : null;
        $this->uuid = isset($data['uuid']) ? $data['uuid'] : null;
    }

    /**
     * Gets status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets status.
     *
     * @param string|null $status
     *
     * @return $this
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Gets datetime.
     *
     * @return string|null
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Sets datetime.
     *
     * @param string|null $datetime  Дата принятия документа на регистрацию
     *
     * @return $this
     */
    public function setDatetime($datetime = null)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Gets uuid.
     *
     * @return string|null
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Sets uuid.
     *
     * @param string|null $uuid  Уникальный идентификатор документа в системе Check-Sender
     *
     * @return $this
     */
    public function setUuid($uuid = null)
    {
        $this->uuid = $uuid;

        return $this;
    }
}


