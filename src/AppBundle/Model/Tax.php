<?php
namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Tax
{
        /**
     * @var string|null
     * @SerializedName("type")
     * @Assert\Choice({ "none", "tax0", "tax10", "tax18", "tax110", "tax118" })
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $type;

    /**
     * @var float|null
     * @SerializedName("sum")
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $sum;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->type = isset($data['type']) ? $data['type'] : null;
        $this->sum = isset($data['sum']) ? $data['sum'] : null;
    }

    /**
     * Gets type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets type.
     *
     * @param string|null $type
     *
     * @return $this
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets sum.
     *
     * @return float|null
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Sets sum.
     *
     * @param float|null $sum
     *
     * @return $this
     */
    public function setSum($sum = null)
    {
        $this->sum = $sum;

        return $this;
    }
}


