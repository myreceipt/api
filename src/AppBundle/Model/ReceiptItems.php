<?php
namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class ReceiptItems
{
    /**
     * Номенклатура
     *
     * @var string
     * @SerializedName("name")
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $name;

    /**
     * Цена, коп.
     *
     * @var float
     * @SerializedName("price")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $price;

    /**
     * Кол-во
     *
     * @var float
     * @SerializedName("quantity")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $quantity;

    /**
     * Сумма
     *
     * @var float
     * @SerializedName("sum")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $sum;

    /**
     * Налог
     *
     * @var Tax|null
     * @SerializedName("tax")
     * @Assert\Type("AppBundle\Model\Tax")
     * @Type("AppBundle\Model\Tax")
     */
    protected $tax;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->price = isset($data['price']) ? $data['price'] : null;
        $this->quantity = isset($data['quantity']) ? $data['quantity'] : null;
        $this->sum = isset($data['sum']) ? $data['sum'] : null;
        $this->tax = isset($data['tax']) ? $data['tax'] : null;
    }

    /**
     * Gets name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name.
     *
     * @param string $name  Номенклатура
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets price.
     *
     * @param float $price  Цена, коп.
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Gets quantity.
     *
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Sets quantity.
     *
     * @param float $quantity  Кол-во
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Gets sum.
     *
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Sets sum.
     *
     * @param float $sum  Сумма
     *
     * @return $this
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Gets tax.
     *
     * @return Tax|null
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Sets tax.
     *
     * @param Tax|null $tax  Налог
     *
     * @return $this
     */
    public function setTax(Tax $tax = null)
    {
        $this->tax = $tax;

        return $this;
    }
}


