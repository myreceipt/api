<?php
namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;


class Receipt 
{
    /**
     * Уникальный идентификатор транзакции в системе клиента, для избежания повторной регистрации чека
     *
     * @var string
     * @SerializedName("id")
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $id;

    /**
     * @var string
     * @SerializedName("type")
     * @Assert\NotNull()
     * @Assert\Choice({ "sell", "buy", "refund_buy", "refund_sell" })
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $type;

    /**
     * @var ReceiptClient
     * @SerializedName("client")
     * @Assert\NotNull()
     * @Assert\Type("AppBundle\Model\ReceiptClient")
     * @Type("AppBundle\Model\ReceiptClient")
     */
    protected $client;

    /**
     * Позиции в чеке
     *
     * @var ReceiptItems[]
     * @SerializedName("items")
     * @Assert\NotNull()
     * @Assert\All({
     *   @Assert\Type("AppBundle\Model\ReceiptItems")
     * })
     * @Type("array<AppBundle\Model\ReceiptItems>")
     */
    protected $items;

    /**
     * @var Tax[]
     * @SerializedName("taxes")
     * @Assert\All({
     *   @Assert\Type("AppBundle\Model\Tax")
     * })
     * @Type("array<AppBundle\Model\Tax>")
     */
    protected $taxes;

    /**
     * Итоговая сумма чека, коп.
     *
     * @var float
     * @SerializedName("total")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Type("float")
     */
    protected $total;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->type = isset($data['type']) ? $data['type'] : null;
        $this->client = isset($data['client']) ? $data['client'] : null;
        $this->items = isset($data['items']) ? $data['items'] : null;
        $this->taxes = isset($data['taxes']) ? $data['taxes'] : null;
        $this->total = isset($data['total']) ? $data['total'] : null;
    }

    /**
     * Gets id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param string $id  Уникальный идентификатор транзакции в системе клиента, для избежания повторной регистрации чека
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets type.
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets client.
     *
     * @return ReceiptClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Sets client.
     *
     * @param ReceiptClient $client
     *
     * @return $this
     */
    public function setClient(ReceiptClient $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Gets items.
     *
     * @return ReceiptItems[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Sets items.
     *
     * @param ReceiptItems[] $items  Позиции в чеке
     *
     * @return $this
     */
    public function setItems(array $items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * Gets taxes.
     *
     * @return Tax[]|null
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * Sets taxes.
     *
     * @param Tax[] $taxes
     *
     * @return $this
     */
    public function setTaxes(array $taxes)
    {
        $this->taxes = $taxes;

        return $this;
    }

    /**
     * Gets total.
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Sets total.
     *
     * @param float $total  Итоговая сумма чека, коп.
     *
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }
}


