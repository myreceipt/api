<?php
namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class DeviceStatusResponse
{
    /**
     * Серийный номер ККТ
     *
     * @var string|null
     * @SerializedName("serial_number")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $serialNumber;

    /**
     * Серийный номер фискального накопителя
     *
     * @var string|null
     * @SerializedName("fiscal_serial_number")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $fiscalSerialNumber;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->serialNumber = isset($data['serialNumber']) ? $data['serialNumber'] : null;
        $this->fiscalSerialNumber = isset($data['fiscalSerialNumber']) ? $data['fiscalSerialNumber'] : null;
    }

    /**
     * Gets serialNumber.
     *
     * @return string|null
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Sets serialNumber.
     *
     * @param string|null $serialNumber  Серийный номер ККТ
     *
     * @return $this
     */
    public function setSerialNumber($serialNumber = null)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Gets fiscalSerialNumber.
     *
     * @return string|null
     */
    public function getFiscalSerialNumber()
    {
        return $this->fiscalSerialNumber;
    }

    /**
     * Sets fiscalSerialNumber.
     *
     * @param string|null $fiscalSerialNumber  Серийный номер фискального накопителя
     *
     * @return $this
     */
    public function setFiscalSerialNumber($fiscalSerialNumber = null)
    {
        $this->fiscalSerialNumber = $fiscalSerialNumber;

        return $this;
    }
}


