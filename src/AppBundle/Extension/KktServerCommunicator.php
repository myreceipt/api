<?php

namespace AppBundle\Extension;


use AppBundle\Entity\Kkt;
use Psr\Log\LoggerInterface;
use AppBundle\Model\ReceiptRequest;

class KktServerCommunicator
{

    const AVAILABLE_DEVICES_COMMAND = 'available_devices';
    const DEVICE_STATUS_COMMAND = 'device_status';
    const DEVICE_NUMBER_COMMAND = 'device_number';
    const FISCAL_STATUS_COMMAND = 'fiscal_status';
    const REGISTRATION_INFO_COMMAND = 'registration_info';
    const SESSION_INFO_COMMAND = 'session_info';
    const SESSION_OPEN_COMMAND = 'session_open';
    const SESSION_CLOSE_COMMAND = 'session_close';
    const SESSION_CANCEL_DOC_COMMAND = 'session_cancel_doc';
    const RECEIPT_COMPOSE_COMMAND = 'receipt_compose';

    const COMMANDS = [
        self::DEVICE_STATUS_COMMAND, self::DEVICE_NUMBER_COMMAND, self::FISCAL_STATUS_COMMAND,
        self::REGISTRATION_INFO_COMMAND, self::SESSION_INFO_COMMAND, self::SESSION_OPEN_COMMAND,
        self::SESSION_CLOSE_COMMAND, self::SESSION_CANCEL_DOC_COMMAND, self::RECEIPT_COMPOSE_COMMAND,
    ];

    /** @var LoggerInterface */
    private $logger;
    /** @var KktServerInterpreter */
    private $interpreter;
    private $host;
    private $port;

    public function __construct(LoggerInterface $logger, KktServerInterpreter $interpreter, $host, $port)
    {
        $this->logger = $logger;
        $this->interpreter = $interpreter;
        $this->host = $host;
        $this->port = $port;
    }

    public function availableDevices($kkt = '')
    {
        return $this->send(self::AVAILABLE_DEVICES_COMMAND, $kkt);
    }

    public function sessionOpen($kkt)
    {
        return $this->send(self::SESSION_OPEN_COMMAND, $kkt);
    }

    public function sessionClose($kkt)
    {
        return $this->send(self::SESSION_CLOSE_COMMAND, $kkt);
    }

    public function sessionInfo($kkt)
    {
        return $this->send(self::SESSION_INFO_COMMAND, $kkt);
    }

    public function deviceStatus($kkt)
    {
        return [
            'device' => $this->send(self::DEVICE_STATUS_COMMAND, $kkt),
            'fiscal' => $this->send(self::FISCAL_STATUS_COMMAND, $kkt),
//            'registration' => $this->send(self::REGISTRATION_INFO_COMMAND, $kkt),
        ];
    }

    public function receiptRegister(ReceiptRequest $receipt, $kkt) {
        // TODO: Удалить этот хардкод, все переписать на объектную модель
        $items = [];
        $taxesDict = [
            'none' => 6,
            'tax0' => 5,
            'tax10' => 2,
            'tax18' => 1,
            'tax110' => 4,
            'tax118' => 3,
        ];
        $typesDict = [
            "sell" => 1,
            "buy" => 3,
            "refund_buy" => 4,
            "refund_sell" => 2
        ];
        foreach ($receipt->getItems() as $item){
            $items[] = [
                'name' => $item->getName(),
                'price' => $item->getPrice(),
                'tax' => $taxesDict[$item->getTax()->getType()],  // Dictionary.taxValues
                'count' => $item->getQuantity()
            ];
        }

        $data = [
            'items' => $items,
            'sign' => $typesDict[$receipt->getType()], // Dictionary.receiptSigns
            'tax_mode' => 2, // Dictionary.taxModes
            'email' => $receipt->getClient()->getEmail(),
            'total' => $receipt->getTotal()
        ];

        return $this->send(self::RECEIPT_COMPOSE_COMMAND, $kkt, $data);
    }

    private function send(string $method, string $kkt, $data = [])
    {
        $response = $this->sendRequest([
            'method' => $method,
            'kkt_number' => $kkt,
            'data' => $data
        ]);

        return $this->interpreter->handle($method, $response);
    }

    /**
     * @param $request
     * example ['method' => '', 'kkt_number' => '', 'data' => []]
     * @return mixed
     */
    private function sendRequest($request)
    {
        $fp = fsockopen("tcp://{$this->host}", $this->port, $errno, $errstr);
        fwrite($fp, json_encode($request));
        $responseData = '';
        while(!feof($fp))
            $responseData = $responseData . fread($fp, 16);
        fclose($fp);
        $response = json_decode($responseData, true);
        $this->logger->notice('KktServerCommunicator log kkt server request', ['request' => $request, 'response' => $response]);
        return $response;
    }

//        composer config "platform.ext-mongo" "1.6.16" && composer require "alcaeus/mongo-php-adapter"

}