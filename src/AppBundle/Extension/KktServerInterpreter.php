<?php
/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 10/02/2018
 * Time: 23:03
 */

namespace AppBundle\Extension;

use Psr\Log\LoggerInterface;

class KktServerInterpreter
{

    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function handle($method, $response)
    {
        switch ($method) {
            case KktServerCommunicator::RECEIPT_COMPOSE_COMMAND: {
                $code = $response['errorCode'] ?? 0;
                $result = [
                    'code' => intval($code),
                    'message' => $response['message'] ?? null,
                ];
                if ($code === 0) {
                    $result['data'] = [
                        'session_receipt_number' => $response[0], // 'Номер чека за смену: %s ' % status[0]
                        'fiscal_number' => $response[1], //'Номер ФД: %s ' % status[1]
                        'fiscal_sign' => $response[2], //'Фискальный признак: %s ' % status[2]
                        //'Дата-Время формирования чека: %s' % datetime.datetime(2000 + status[3], status[4], status[5], status[6], status[7])
                        'fiscal_date' => $response[5].'.'.$response[4].'.'.strval(2000 + $response[3]).' '.$response[6].':'.$response[7]
                    ];
                }
                break;
            }
            default: {
                $result = $response;
            }
        }
        return $result;
    }

}