<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Company;
use AppBundle\Entity\Kkt;
use AppBundle\Extension\KktServerCommunicator;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use AppBundle\Model\DeviceStatusResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 08/02/2018
 * Time: 23:15
 */

class StatusController extends ApiBaseController
{

    /** @var KktServerCommunicator */
    protected $communicator;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(LoggerInterface $logger, EntityManager $em, KktServerCommunicator $communicator, TokenStorage $tokenStorage)
    {
        parent::__construct($logger, $em);
        $this->communicator = $communicator;
        $this->tokenStorage = $tokenStorage;
    }

    public function deviceStatus($id)
    {
        $repoC = $this->em->getRepository(Company::class);
        $company = $repoC->findOneBy(['inn' => $this->tokenStorage->getToken()->getUser()->getUsername()]);

        $repo = $this->em->getRepository(Kkt::class);
        $kkt = $repo->findOneBy(['company' => $company, 'id' => $id]);
        if (!$kkt instanceof Kkt)
            $this->accessDenied();

        //$result = $this->communicator->deviceStatus($kkt->getSerialNumber());
        $response = (new DeviceStatusResponse())
                    ->setSerialNumber($kkt->getSerialNumber())
                    ->setFiscalSerialNumber($kkt->getFn()->getSerialNumber());

        return $response;
    }

}