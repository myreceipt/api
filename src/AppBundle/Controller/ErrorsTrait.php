<?php
/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 09/02/2018
 * Time: 10:38
 */

namespace AppBundle\Controller;


use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

trait ErrorsTrait
{

    protected function accessDenied()
    {
        throw new AccessDeniedHttpException('Access Denied');
    }

    protected function unauthorized($token)
    {
        throw new UnauthorizedHttpException($token, 'Unauthorized');
    }

    protected function methodNotAllowed()
    {
        throw new MethodNotAllowedException([], 'Method Not Allowed');
    }

}