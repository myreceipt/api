<?php

namespace AppBundle\Controller;

use AppBundle\Document\FiscalDocument;
use AppBundle\Entity\ApiToken;
use AppBundle\Entity\Company;
use AppBundle\Entity\Kkt;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Makasim\Yadm\Storage;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Psr\Log\LoggerInterface;
use AppBundle\Model\ReceiptRequest;
use AppBundle\Model\ReceiptResponse;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 08/02/2018
 * Time: 23:15
 */
class ReceiptController extends ApiBaseController
{

    /** @var ProducerInterface */
    protected $producer;
    /** @var Serializer */
    protected $serializer;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var Storage */
    private $storage;

    public function __construct(LoggerInterface $logger, EntityManager $em, ProducerInterface $producer, Serializer $serializer, TokenStorage $tokenStorage, Storage $storage)
    {
        parent::__construct($logger, $em);
        $this->producer = $producer;
        $this->serializer = $serializer;
        $this->tokenStorage = $tokenStorage;
        $this->storage = $storage;
    }

    public function compose(ReceiptRequest $request)
    {
        $this->logger->notice("Log User ReceiptRequest", ["receipt_request" => $this->serializer->serialize($request,'json')]);
        $inn = $this->tokenStorage->getToken()->getUser()->getUsername();
        $row = $this->storage->findOne([
            'request.id' => $request->getId(),
            'inn' => $inn,
            'request.type' => $request->getType()
        ]);

        if ($row instanceof FiscalDocument) {
            $response = $this->serializer->fromArray($row->getResponse(), ReceiptResponse::class);
        } else {
            $response = (new ReceiptResponse())->setCode(-1);
            $row = new FiscalDocument();
            $row->setDocument('receipt')
                ->setRequest($this->serializer->toArray($request))
                ->setInn($inn)
                ->setResponse($this->serializer->toArray($response));

            $this->storage->insert($row);
            $this->producer->publish($row->getId());
        }

        $response->setUuid($row->getId());
        return $response;
    }

}