<?php
/**
 * Created by PhpStorm.
 * User: puzakov
 * Date: 17/02/2018
 * Time: 21:25
 */

namespace AppBundle\Controller;


use AppBundle\Entity\ApiToken;
use AppBundle\Entity\Company;
use AppBundle\Entity\Kkt;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

abstract class ApiBaseController
{

    /** @var LoggerInterface */
    protected $logger;
    /** @var EntityManager */
    protected $em;
    /** @var Company */
    protected $company;

    public function __construct(LoggerInterface $logger, EntityManager $em)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function settoken($value)
    {
        $repo = $this->em->getRepository(ApiToken::class);
        $token = $repo->findOneBy(['token' => $value, 'isValid' => true]);

        if (!$token instanceof ApiToken)
            $this->accessDenied();

        $this->company = $token->getCompany();
    }

    protected function chooseKkt()
    {
        $repo = $this->em->getRepository(Kkt::class);
        $kkt = $repo->findOneBy(['company' => $this->company]);
        return $kkt;
    }

    protected function accessDenied()
    {
        throw new AccessDeniedHttpException('Access Denied');
    }

    protected function requestErrors(array $errors)
    {
        throw new MethodNotAllowedHttpException([], $errors[0]);
    }

}