<?php

namespace AppBundle\Command;

use AppBundle\Extension\KktServerCommunicator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class KktServiceCommand extends Command
{

    protected static $defaultName = 'app:kkt-service';

    /** @var LoggerInterface  */
    private $logger;
    /** @var KktServerCommunicator */
    private $communicator;

    public function __construct(LoggerInterface $logger, KktServerCommunicator $communicator)
    {
        $this->logger = $logger;
        parent::__construct();
        $this->communicator = $communicator;
    }

    protected function configure()
    {
        $this
            ->setDescription('...')
            ->addOption('kkt', 'k', InputOption::VALUE_REQUIRED, 'Kkt serial number')
            ->addOption('method', 'm', InputOption::VALUE_REQUIRED, 'Kkt server communicator method')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->debug('Command Start.');
        $kkt = $input->getOption('kkt');
        $method = $input->getOption('method');

        if (empty($kkt) || empty($method) || !method_exists($this->communicator, $method)) {
            $this->logger->info('Arguments required.');
            exit(1);
        }

//        $kkt = '550101000695';

        $result = $this->communicator->$method($kkt);

        $this->logger->debug('Command End.');
    }

}
