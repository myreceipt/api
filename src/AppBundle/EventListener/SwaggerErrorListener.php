<?php

namespace AppBundle\EventListener;

use AppBundle\Model\VndError;
use JMS\Serializer\Serializer;
use KleijnWeb\SwaggerBundle\Exception\InvalidParametersException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SwaggerErrorListener
{
    /** @var Logger */
    private $logger;
    /** @var Serializer */
    private $serializer;

    const DEFAULT_MESSAGE = 'Input Validation Failure';

    /**
     * SwaggerErrorListener constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger, Serializer $serializer)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @throws \Exception
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $headers = ['Content-Type' => 'application/vnd.error+json'];
        $logRef = uniqid();
        $vndError = (new VndError())->setLogref($logRef);

        try {
            $exception = $event->getException();
            $code = $exception->getCode();

            if ($exception instanceof InvalidParametersException) {
                $statusCode = $exception->getCode();
                $vndError->setMessage(empty($exception->getMessage()) ? self::DEFAULT_MESSAGE : $exception->getMessage())
                         ->setErrors($this->buildErrors($exception));
            } else {
                if ($exception instanceof NotFoundHttpException) {
                    $statusCode = Response::HTTP_NOT_FOUND;
                } else {
                    if ($exception instanceof MethodNotAllowedHttpException) {
                        $statusCode = Response::HTTP_METHOD_NOT_ALLOWED;
                    } elseif ($exception instanceof AuthenticationException) {
                        $statusCode = Response::HTTP_UNAUTHORIZED;
                    } else {
                        $is3Digits = strlen($code) === 3;
                        $class     = (int)substr($code, 0, 1);
                        if (!$is3Digits) {
                            $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                        } else {
                            switch ($class) {
                                case 4:
                                    $statusCode = Response::HTTP_BAD_REQUEST;
                                    break;
                                case 5:
                                    $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                                    break;
                                default:
                                    $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                            }
                        }
                    }
                }
                $message  = Response::$statusTexts[$statusCode];
                $vndError->setMessage($message);
            }

            $reference = $logRef ? " [logref $logRef]" : '';
            $this->logger->notice("{$vndError->getMessage()}{$reference}: $exception", ['logref' => $logRef]);
            $event->setResponse(
                new Response($this->serializer->serialize($vndError, 'json'), $statusCode, $headers)
            );
        } catch (\Exception $e) {
            $message  = "Error Handling Failure";
            $vndError = (new VndError())->setMessage($message)
                                        ->setLogref($logRef);

            $this->logger->critical("$message [logref $logRef]: $e", ['logref' => $logRef]);
            $event->setResponse(
                new Response($this->serializer->serialize($vndError, 'json'), Response::HTTP_INTERNAL_SERVER_ERROR, $headers)
            );
        }
    }

    /**
     * @param InvalidParametersException $exception
     *
     * @return array
     */
    public function buildErrors(InvalidParametersException $exception)
    {
        $errors = [];
        foreach ($exception->getValidationErrors() as $errorSpec) {
            if (!$errorSpec['property'])
                $errorSpec['property'] = preg_replace('/the property (.*) is required/', '\\1', $errorSpec['message']);

            $normalizedPropertyName = str_replace('request.', '', preg_replace('/\[\d+\]/', '', $errorSpec['property']));
            $errors[] = sprintf('[%s] %s', $normalizedPropertyName, $errorSpec['message']);
        }

        return !empty($errors) ? $errors : null;
    }
}