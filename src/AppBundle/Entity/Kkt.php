<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kkt
 *
 * @ORM\Table(name="Kkt")
 * @ORM\Entity
 */
class Kkt
{
    /**
     * @var int
     *
     * @ORM\Column(name="KktID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CompanyID", referencedColumnName="CompanyID")
     * })
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="serialNumber", type="string", length=255)
     */
    private $serialNumber;

    /**
     * @var Fn
     * @ORM\ManyToOne(targetEntity="Fn", fetch="EAGER")
     * @ORM\JoinColumn(name="FnID", referencedColumnName="FnID")
     */
    private $fn;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Set serialNumber.
     *
     * @param string $serialNumber
     *
     * @return Kkt
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber.
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @return Fn
     */
    public function getFn()
    {
        return $this->fn;
    }

    /**
     * @param Fn $fn
     * @return $this
     */
    public function setFn(Fn $fn)
    {
        $this->fn = $fn;
        return $this;
    }

}
