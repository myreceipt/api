<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="Company")
 * @ORM\Entity
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="CompanyID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UserID", referencedColumnName="UserID")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="INN", type="string", length=12)
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SNO", type="string", length=12, nullable=true)
     */
    private $sno;

    /**
     * @var string
     *
     * @ORM\Column(name="PaymentAddress", type="string", length=255)
     */
    private $paymentAddress;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Set inn.
     *
     * @param string $inn
     *
     * @return Company
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn.
     *
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set sno.
     *
     * @param string|null $sno
     *
     * @return Company
     */
    public function setSno($sno = null)
    {
        $this->sno = $sno;

        return $this;
    }

    /**
     * Get sno.
     *
     * @return string|null
     */
    public function getSno()
    {
        return $this->sno;
    }

    /**
     * Set paymentAddress.
     *
     * @param string $paymentAddress
     *
     * @return Company
     */
    public function setPaymentAddress($paymentAddress)
    {
        $this->paymentAddress = $paymentAddress;

        return $this;
    }

    /**
     * Get paymentAddress.
     *
     * @return string
     */
    public function getPaymentAddress()
    {
        return $this->paymentAddress;
    }
}
