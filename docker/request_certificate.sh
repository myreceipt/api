#!/bin/bash -v

mkdir -p /tmp/letsencrypt/www/api
docker run -it --rm --name letsencrypt \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
    -v "/tmp/letsencrypt/www/api:/var/www" \
    certbot/certbot:latest auth --authenticator webroot --webroot-path /var/www --renew-by-default --server \
    https://acme-v01.api.letsencrypt.org/directory -d api.check-sender.tk

mkdir -p /tmp/letsencrypt/www/main
docker run -it --rm --name letsencrypt \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
    -v "/tmp/letsencrypt/www/main:/var/www" \
    certbot/certbot:latest auth --authenticator webroot --webroot-path /var/www --renew-by-default --server \
    https://acme-v01.api.letsencrypt.org/directory -d check-sender.tk