#!/bin/bash -v

cd /home/puzakov/api

docker-compose down || true
git pull
docker-compose up -d
docker-compose exec -T --user www-data php rm -Rf var/cache/*
docker-compose exec -T --user www-data php composer install
