#!/bin/bash -v

cd /home/puzakov/api

echo 'Clossing session 550101000879 ...'
docker-compose exec -T --user www-data php bin/console app:kkt-service --method sessionClose --kkt 550101000879 --env=prod --no-debug -vv
echo 'Opening session 550101000879 ...'
docker-compose exec -T --user www-data php bin/console app:kkt-service --method sessionOpen --kkt 550101000879 --env=prod --no-debug -vv

echo 'Clossing session 550101000695 ...'
docker-compose exec -T --user www-data php bin/console app:kkt-service --method sessionClose --kkt 550101000695 --env=prod --no-debug -vv
echo 'Opening session 550101000695 ...'
docker-compose exec -T --user www-data php bin/console app:kkt-service --method sessionOpen --kkt 550101000695 --env=prod --no-debug -vv
